

<?php
	// Database connection 
	//echo phpinfo(); exit;

	$servername = "localhost";
	$username = "seaqdiqp_sea_quasar";
	$password = "G!~)E9kQ3YB0";
	$dbname = "seaqdiqp_contactdb";
	
if ($_SERVER["REQUEST_METHOD"] == "POST") {

	// Create connection
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	// Check connection
	if (!$conn) {
	  die("Connection failed: " . mysqli_connect_error());
	} else{
		//  exit;
			
		  $name = inputeValidation($_POST["name"]);
		  $email = inputeValidation($_POST["email"]);
		  $subject = inputeValidation($_POST["subject"]);
		  $message = inputeValidation($_POST["message"]);
		  $date = date('Y-m-d H:i:s');
	    	
		//$sql = "INSERT INTO contact_us(name, email, subject, message, status,date) VALUES ($name, $email,$subject,$message,1,$date)";
		$sql = "INSERT INTO contact_us (id, name, email, subject, message, status, date) VALUES (NULL, '".$name."', '".$email."', '".$subject."', '".$message."', '1', '".$date."')";
    
		if (mysqli_query($conn, $sql)) {
		    /*
		  $message = "Your message has been sent. We will contact you. Thank you!";
          echo "<script type='text/javascript'>alert('$message');</script>";
          exit;
          
          */
          
          echo 'OK'; exit;

          
		} else {
		  echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		} 
	}
	mysqli_close($conn);
  
}

function inputeValidation($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Sea Quasar</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">


</head>

<body>
  <!--==========================
              Header
  ============================-->
  <header id="header">

    <div id="topbar">
      <div class="container">
        <div class="social-links">
          <a href="https://twitter.com/QuasarSea" class="twitter"><i class="fa fa-twitter"></i></a>
          <a href="https://www.facebook.com/seaquasar" class="facebook"><i class="fa fa-facebook"></i></a>
          <a href="https://www.linkedin.com/company/seaquasar" class="linkedin"><i class="fa fa-linkedin"></i></a>
          <a href="https://www.instagram.com/seaquasar/" class="instagram"><i class="fa fa-instagram"></i></a>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="logo float-left logo-gap-right">
        <img src="img/logo.png" alt="" class="img-fluid float-left logo-l"/>
        <h1 class="text-light logo-text" style="white-space: nowrap;"><a href="#intro" class="scrollto"><span>Sea Quasar</span></a></h1>
      </div>
      

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="#intro">Home</a></li>
          <li><a href="#about">About Us</a></li>
          <li><a href="#services">Services</a></li> 
          <li><a href="#team">Team</a></li>
          <li><a href="#footer">Contact Us</a></li>
        </ul>
      </nav>
    </div>
  </header>
  
  <!-- #header -->

  <!--==========================
           Intro Section
  ============================-->
  <section id="intro" class="clearfix">
    <div class="container d-flex h-100">
      <div class="row justify-content-center align-self-center">
        <div class="col-md-4 intro-info order-md-first order-last motto">
          <h2 ><span>Innovation</span> <br>is our motto!</h2>
          <div>
            <a href="#about" class="btn-get-started scrollto">Get Started</a>
          </div>
        </div>
  
        <div class="col-md-8 intro-img order-md-last order-first">
          <img src="img/image.png" alt="" class="img-fluid">
        </div>
      </div>

    </div>
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
            About Us Section
    ============================-->
    <section id="about">
      <div class="container">

        <header class="section-header">
          <h3>About Us</h3>
          <p>A dedicated team to hire to achieve your goal. We are focused and passionate about using the best technologies available in the market. Focusing the new technologies we are determined to try to provide an exclusive solution to any problem.</p>
        </header>

        <div class="row about-cols">

          <div class="col-md-4 wow fadeInUp">
            <div class="about-col">
              <div class="img">
                <img src="img/about-mission.jpg" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Our Mission</a></h2>
              <p>
                A courage of solving new problems or fulfilling your dreams through web. 
              </p>
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
            <div class="about-col">
              <div class="img">
                <img src="img/about-plan.jpg" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-list-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Our Vision</a></h2>
              <p>
                We are aimed to make a footstep on this era to solve creative challenges.
              </p>
            </div>
          </div>

          <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
            <div class="about-col">
              <div class="img">
                <img src="img/about-vision.jpg" alt="" class="img-fluid">
                <div class="icon"><i class="ion-ios-eye-outline"></i></div>
              </div>
              <h2 class="title"><a href="#">Our Values</a></h2>
              <p class="description text-justify">
                   
                  Be a team player, collaborate, have fun.
                  Get out of your comfort zone.
                  Be part of the solution.
                 
                
              
              </p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #about -->


    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Services</h3>
          <center><h4>We are exicting to work with the following services of which we are good at</h4>  </center>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon" style="background: #fceef3;"><i class="ion-ios-analytics-outline" style="color: #ff689b;"></i></div>
              <h4 class="title"><a href="">Business Automation</a></h4>
               <p class="description text-justify"> <span class="more">We are offering our services to the small and big businesses to reduce  their workloads in a simplier and cost effective way.  </span> </p> 
            
            </div>
          </div>
          <div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon" style="background: #fff0da;"><i class="ion-ios-bookmarks-outline" style="color: #e98e06;"></i></div>
              <h4 class="title"><a href="">Web Application</a></h4>
              <p class="description text-justify">By adapting topnotch technologies affiliated with this era to make an impact.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon" style="background: #e6fdfc;"><i class="ion-ios-paper-outline" style="color: #3fcdc7;"></i></div>
              <h4 class="title"><a href="">Mobile Application</a></h4>
              <p class="description text-justify">We create mobile applications of any complexity for B2C and B2B use cases.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="wow fadeIn">
      <div class="container-fluid">
        
        <header class="section-header">
          <h3>Why choose us?</h3>
          <center> <h4>We have highly skilled engineers with excellent technical knowledge and experience in using the latest software standards </h4> </center>
        </header>


      </div>

    </section>

    <!--==========================
      Testimonials Section
    ============================-->
    <!-- #testimonials -->

    <!--==========================
      Team Section
    ============================-->
    <section id="team" class="section-bg">
      <div class="container">
        <div class="section-header">
          <h3>Team</h3>
          <center><h4>These are the people that make the magic happen </center></h4>
        </div>

        <div class="row">

          <div class="col-lg-4 col-md-7 wow fadeInUp">
            <div class="member">
              <img src="img/team1.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Atikul Islam</h4>
                  <span>Chief Executive Officer</span>
                  <div class="social">
                    <a href="https://www.linkedin.com/in/atikul-i-95325911b/"><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-7 wow fadeInUp" data-wow-delay="0.1s">
            <div class="member">
              <img src="img/team-2.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Abdullah Al Noman</h4>
                  <span>Product Manager</span>
                  <div class="social">
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-7 wow fadeInUp" data-wow-delay="0.2s">
            <div class="member">
              <img src="img/team3.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Emon Hossain</h4>
                  <span>CTO</span>
                  <div class="social">
                    <a href="https://www.linkedin.com/in/emonhossain/"><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>



        </div>

      </div>
    </section><!-- #team -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer" class="section-bg">
    <section id="contact" class="section-bg wow fadeInUp">
      <div class="container">

  <!--==========================
    Contact Us
  ============================-->
    
        <div class="section-header">
          <h3>Contact Us</h3>
          <p>Want to get in touch? We would love to hear form you. Here's how you can reach us...</p>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Address</h3>
              <address>Reichenhainar Str. 51, Chemnitz</address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Phone Number</h3>
              <p><a href="tel:+4917658869381">+49 17658869381</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:seaquasar@outlook.com">seaquasar@outlook.com</a></p>
            </div>
          </div>

        </div>
     

        <div class="form">
          <div id="sendmessage">Your message has been sent. Thank you!</div>
          <div id="errormessage"></div>
          <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" role="form" class="contactForm" id="From">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center"><button type="submit" >Send Message</button></div>
          </form>
        </div>

      </div>
    </section>
    
    <script>
    function myFunction() {
      document.getElementById("contactForm").reset();
    }
    </script>
 
    <!-- #contact -->

    <div class="container">
      <div class="copyright">
       <strong>Disclaimer: "This website does not belong to a real company. It is a Planspiel Web Engineering project."</strong>
      </div>
    </div>
  </footer><!-- #footer -->
  
   
   
   <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <p id="success_message">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/mobile-nav/mobile-nav.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
